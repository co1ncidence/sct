CC     ?= gcc
CFLAGS += -Wall -Wextra -Werror -pedantic -std=c99 -O2 -I /usr/X11R6/include
LDFLAGS = -L /usr/X11R6/lib -lX11 -lXrandr -lm -s

NAME = sct
BIN  = bin/$(NAME)
MAN  = doc/$(NAME).1

PREFIX ?= $(DESTDIR)/usr
BINDIR  = $(PREFIX)/bin
MANDIR  = $(PREFIX)/share/man/man1

all : $(BIN)

bin/% : src/%.c
	@mkdir -p bin
	$(CC) $(CFLAGS) $< -o $@ $(LDFLAGS)

clean :
	rm -rf bin

install : all
	install -Dm755 $(BIN) -t $(BINDIR)
	install -m 0644 doc/$(NAME).1 $(MANDIR)

uninstall :
	rm -f $(BINDIR)/$(NAME)
	rm -f $(MANDIR)/$(NAME).1

.PHONY : all clean install uninstall
